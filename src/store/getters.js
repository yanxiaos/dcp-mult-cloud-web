/*
 * @Description: getter汇总文件
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-06-15 08:51:36
 * @LastEditors: John Holl
 * @LastEditTime: 2021-06-15 11:16:17
 */

const getters = {
  // sidebar: state => state.app.sidebar
  activeMenu: state => state.app.activeMenu  // 当前激活菜单
}

export default getters